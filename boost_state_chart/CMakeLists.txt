cmake_minimum_required(VERSION 3.13.4)
project(StateChartTest)

enable_testing()

# Define compiler switches
set(CMAKE_CXX_FLAGS "-ggdb -g3 -Wall -std=c++17")

# This sets the output dir to /bin
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)

include_directories(${CMAKE_SOURCE_DIR}/boost_1_76_0/)

add_executable(StateChartTest
                src/main.cpp
                src/retosTests.cpp
                src/retosTestsHierarchical.cpp
                src/retosTestsUnsupportedTransactions.cpp
                src/wrappedStateMachine.cpp
                src/simpleExample.cpp
                src/stopWatchExample.cpp)

set_target_properties(StateChartTest
    PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED ON
    CXX_EXTENSIONS ON)

target_link_libraries(StateChartTest)
