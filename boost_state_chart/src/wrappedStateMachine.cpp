// (c) red
#include <boost/mpl/list.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/transition.hpp>
#include <iostream>

#define BSM_DECLARE_EVENT(eventName)  struct eventName : sc::event<eventName> {}
#define BSM_DECLARE_STATE(stateName)  struct stateName

// #define BSM_STATE_MACHINE(machineName, initialState) struct machineName : sc::state_machine<machineName, initialState> \
// { \
//     void unconsumed_event( const sc::event_base & ) { \
//         std::cout << "   UNSUPPORTED EVENT !!!!!!!!!!!!!!!!!!!!" << std::endl; \
//         /*std::cout << "     current state: " << state.c_str() << std::endl; */ \
//         std::cout << "   UNSUPPORTED EVENT !!!!!!!!!!!!!!!!!!!!" << std::endl; \
//         /*throw std::runtime_error( "Event was not consumed!" );*/      \
//     } \
// \
//     void log_process_event(const sc::event_base&) {                         \
//     std::cout << "   xxxx" << std::endl; \
//   } \
// bool isState1()  {                         \
//     return true; \
// } \
// }


namespace WrappedStateMachine {

namespace sc = boost::statechart;

// Define all events
BSM_DECLARE_EVENT(EvButton12);
BSM_DECLARE_EVENT(EvButton22);
BSM_DECLARE_EVENT(EvButton23);
BSM_DECLARE_EVENT(NotSupported);


// Forward definition of states
BSM_DECLARE_STATE(State1);
BSM_DECLARE_STATE(State2);
BSM_DECLARE_STATE(State3);

// The state machine
struct MachineUnsupportedStates : sc::state_machine<MachineUnsupportedStates, State1>
{
    void unconsumed_event(const sc::event_base & currentEvent)
    {
        std::cout << "   >>>>> UNSUPPORTED EVENT" << std::endl;
        MachineUnsupportedStates::state_iterator pState = this->state_begin();
        std::cout << "     current state: " << typeid( *pState ).name() << std::endl;
        std::cout << "     event: " << typeid(currentEvent).name() << std::endl;
        std::cout << "   <<<<< UNSUPPORTED EVENT" << std::endl;
        /*throw std::runtime_error( "Event was not consumed!" );*/
    }


    void process_event(const sc::event_base & currentEvent)
    {
        MachineUnsupportedStates::state_iterator pState = this->state_begin();
        std::string oldState = typeid(*pState).name();
        state_machine::process_event(currentEvent);
        MachineUnsupportedStates::state_iterator newState = this->state_begin();
        std::cout << "-> " << typeid(currentEvent).name() << " from: " << oldState << " to: " << typeid(*newState).name() << std::endl;
    }


    /// Get state or state group
    bool readyToHome()
    {
        if (state_downcast<const State1*>() != 0 ) {
            return true;
        }
        return false;
    }

    void printActiveState()
    {
        //- for (MachineUnsupportedStates::state_iterator pState = this->state_begin(); pState != this->state_end(); ++pState ){
        //-     std::cout << "   xxx state: " << typeid( *pState ).name() << std::endl;
        //- }
        MachineUnsupportedStates::state_iterator pState = this->state_begin();
        std::cout << "   printActiveState: " << typeid( *pState ).name() << std::endl;
    }
};

// States and its events
struct State1 : sc::simple_state<State1, MachineUnsupportedStates>
{
    // Define all transitions
    typedef sc::transition<EvButton12, State2> reactions;

    State1()
    {
        std::cout << "   Enter State1" << std::endl;
    }
    ~State1()
    {
        std::cout << "   Exit State1" << std::endl;
    }    // exit
};

struct State2 : sc::simple_state<State2, MachineUnsupportedStates>
{
    using reactions = boost::mpl::list<sc::custom_reaction<EvButton22>, sc::custom_reaction<EvButton23>>;
    State2()
    {
        std::cout << "   Enter State2" << std::endl;
    }
    ~State2()
    {
        std::cout << "   Exit State2" << std::endl;
    }
    sc::result react(const EvButton22& evt)
    {
        std::cout << "   remain in state" << std::endl;
        return discard_event();
    }
    sc::result react(const EvButton23& evt)
    {
        std::cout << "   transit to 3" << std::endl;
        post_event(evt);
        return transit<State3>();
    }
};

struct State3 : sc::simple_state<State3, MachineUnsupportedStates>
{
    State3()
    {
        std::cout << "   Enter State3" << std::endl;
    }
    ~State3()
    {
        std::cout << "   Exit State3" << std::endl;
    }
};

void wrappedStateMachine()
{
    MachineUnsupportedStates myMachine;
    myMachine.initiate();

    myMachine.printActiveState();

    std::cout << "   ev12" << std::endl;
    std::cout << "   readyToHome: " << myMachine.readyToHome() << std::endl;
    myMachine.process_event(EvButton12());
    std::cout << "   readyToHome: " << myMachine.readyToHome() << std::endl;
    std::cout << "   ev12 again" << std::endl;
    myMachine.process_event(EvButton12());    // event is ignored -> and an error dumped!
    myMachine.process_event(NotSupported());    // event is ignored -> and an error dumped!

    std::cout << "   Continue" << std::endl;
    myMachine.process_event(EvButton22());
    myMachine.process_event(EvButton23());

    myMachine.printActiveState();

    std::cout << "   Desctruct ..." << std::endl;
}

};  // namespace WrappedStateMachine
