#include <boost/mpl/list.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/transition.hpp>
#include <iostream>

#define DUMP_ENTER_STATE std::cout << "Enter " << BOOST_CURRENT_FUNCTION << std::endl
#define DUMP_EXIT_STATE  std::cout << "Exit " << BOOST_CURRENT_FUNCTION << std::endl
#define DUMP_EVENT       std::cout << "Process " << BOOST_CURRENT_FUNCTION << std::endl

namespace HierarchicalExample {

namespace sc = boost::statechart;

// -----------------------------------------------------
// Define all events
struct EvToSubB : sc::event<EvToSubB>
{
};
struct EvToMain2 : sc::event<EvToMain2>
{
};

// -----------------------------------------------------
// Define all states
struct MainState1;
struct MainState2;
// Substates to 1
struct SubState1A;
struct SubState1B;
struct SubState1C;

// -----------------------------------------------------
struct RetosMachine : sc::state_machine<RetosMachine, MainState1>
{
};

struct MainState1 : sc::simple_state<MainState1, RetosMachine, SubState1A>
{
    // Define all transitions
    // typedef sc::transition<EvButton12, State2> reactions;
    MainState1()
    {
        DUMP_ENTER_STATE;
    }
    ~MainState1()
    {
        DUMP_EXIT_STATE;
    }
};


struct MainState2 : sc::simple_state<MainState2, RetosMachine>
{
    MainState2()
    {
        DUMP_ENTER_STATE;
    }
    ~MainState2()
    {
        DUMP_EXIT_STATE;
    }
};

struct SubState1A : sc::simple_state<SubState1A, MainState1>
{
    using reactions = boost::mpl::list<sc::custom_reaction<EvToSubB>>;

    SubState1A()
    {
        DUMP_ENTER_STATE;
    }
    ~SubState1A()
    {
        DUMP_EXIT_STATE;
    }
    sc::result react(const EvToSubB& evt)
    {
        DUMP_EVENT;
        return transit<SubState1B>();
    }
};

struct SubState1B : sc::simple_state<SubState1B, MainState1>
{
    using reactions = boost::mpl::list<sc::custom_reaction<EvToMain2>>;
    SubState1B()
    {
        DUMP_ENTER_STATE;
    }
    ~SubState1B()
    {
        DUMP_EXIT_STATE;
    }
    sc::result react(const EvToMain2& evt)
    {
        DUMP_EVENT;
        return transit<MainState2>();
    }
};

struct SubState1C : sc::simple_state<SubState1C, MainState1>
{
    SubState1C()
    {
        DUMP_ENTER_STATE;
    }
    ~SubState1C()
    {
        DUMP_EXIT_STATE;
    }
};

#if 0
// Templates:
    sc::result react(const EvButton56b& evt)
    {
        std::cout << "transit to 6b" << std::endl;
        post_event(evt);
        return transit<State6b>();
    }
    sc::result react(const EvButton56a& evt)
    {
        std::cout << "got msg 56a" << std::endl;
        return discard_event();
    }
    using reactions = boost::mpl::list<sc::custom_reaction<EvButton56a>, sc::custom_reaction<EvButton56b>>;
#endif

void retosHierarchicalExample()
{
    RetosMachine myMachine;
    myMachine.initiate();

    myMachine.process_event(EvToSubB());
    myMachine.process_event(EvToMain2());

    std::cout << "Desctruct ..." << std::endl;
}

}; // namespace HierarchicalExample
