// (c) red
#include <boost/mpl/list.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/transition.hpp>
#include <iostream>

namespace UnsupportedEvents {

namespace sc = boost::statechart;

// Define all events
struct EvButton12 : sc::event<EvButton12>
{
};
struct EvButton22 : sc::event<EvButton22>
{
};
struct EvButton23 : sc::event<EvButton23>
{
};
struct NotSupported : sc::event<NotSupported>
{
};


// Forward definition of states
struct State1;
struct State2;
struct State3;

// The state machine
struct MachineUnsupportedStates : sc::state_machine<MachineUnsupportedStates, State1>
{
    void unconsumed_event( const sc::event_base & ) {
        std::cout << "UNSUPPORTED EVENT !!!!!!!!!!!!!!!!!!!!" << std::endl;
        //throw std::runtime_error( "Event was not consumed!" );
    }
};


// States and its events
struct State1 : sc::simple_state<State1, MachineUnsupportedStates>
{
    // Define all transitions
    typedef sc::transition<EvButton12, State2> reactions;

    State1()
    {
        std::cout << "Enter State1" << std::endl;
    }
    ~State1()
    {
        std::cout << "Exit State1" << std::endl;
    }    // exit
};

#if 0
struct State2 : sc::simple_state<State2, MachineUnsupportedStates>
{
    typedef boost::mpl::list<sc::transition<EvButton22, State2>, sc::transition<EvButton23, State3>> reactions;
    State2()
    {
        std::cout << "Enter State2" << std::endl;
    }
    ~State2()
    {
        std::cout << "Exit State2" << std::endl;
    }
};
#else
struct State2 : sc::simple_state<State2, MachineUnsupportedStates>
{
    using reactions = boost::mpl::list<sc::custom_reaction<EvButton22>, sc::custom_reaction<EvButton23>>;
    State2()
    {
        std::cout << "Enter State2" << std::endl;
    }
    ~State2()
    {
        std::cout << "Exit State2" << std::endl;
    }
    sc::result react(const EvButton22& evt)
    {
        std::cout << "remain in state" << std::endl;
        return discard_event();
    }
    sc::result react(const EvButton23& evt)
    {
        std::cout << "transit to 3" << std::endl;
        post_event(evt);
        return transit<State3>();
    }
};
#endif

struct State3 : sc::simple_state<State3, MachineUnsupportedStates>
{
    State3()
    {
        std::cout << "Enter State3" << std::endl;
    }
    ~State3()
    {
        std::cout << "Exit State3" << std::endl;
    }
    //sc::result react(const EvButton56a& evt)
    //{
    //    std::cout << "transit to 6a" << std::endl;
    //    post_event(evt);
    //    return transit<State3a>();
    //}
    //sc::result react(const EvButton56b& evt)
    //{
    //    std::cout << "transit to 6b" << std::endl;
    //    post_event(evt);
    //    return transit<State3b>();
    //}
    //using reactions = boost::mpl::list<sc::custom_reaction<EvButton56a>, sc::custom_reaction<EvButton56b>>;
};

void retosUnsupportedTransactions()
{
    MachineUnsupportedStates myMachine;
    myMachine.initiate();

    std::cout << "ev12" << std::endl;
    myMachine.process_event(EvButton12());
    std::cout << "ev12 again" << std::endl;
    myMachine.process_event(EvButton12());    // event is ignored -> and an error dumped!
    std::cout << "ev not supported at all" << std::endl;
    myMachine.process_event(NotSupported());    // event is ignored -> and an error dumped!

    std::cout << "Continue" << std::endl;
    myMachine.process_event(EvButton22());
    myMachine.process_event(EvButton23());


    std::cout << "Desctruct ..." << std::endl;
}

};  // namespace UnsupportedEvents
