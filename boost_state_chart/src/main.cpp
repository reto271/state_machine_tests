#include "retosTests.h"
#include "retosTestsHierarchical.h"
#include "retosTestsUnsupportedTransactions.h"
#include "simpleExample.h"
#include "stopWatchExample.h"
#include "wrappedStateMachine.h"

#include <iostream>


int main()
{
    std::cout << "Run simple example" << std::endl;
    SimpleExample::simpleExample();
    std::cout << "Done simple example" << std::endl << std::endl;

    std::cout << "Run stop watch example" << std::endl;
    StopWatch::stopWatchExample();
    std::cout << "Done stop watch example" << std::endl << std::endl;

    std::cout << "Run reto's example" << std::endl;
    RetosBasicExamples::retosExample();
    std::cout << "Done reto's example" << std::endl << std::endl;

    std::cout << "Run reto's hierarchical example" << std::endl;
    HierarchicalExample::retosHierarchicalExample();
    std::cout << "Done reto's hierarchical example" << std::endl << std::endl;

    std::cout << "-----------------------" << std::endl;
    std::cout << "Run reto's unsupported transactions" << std::endl;
    UnsupportedEvents::retosUnsupportedTransactions();
    std::cout << "Done reto's unsupported transactions" << std::endl << std::endl;

    std::cout << "-----------------------" << std::endl;
    std::cout << "Run wrapped SM" << std::endl;
    WrappedStateMachine::wrappedStateMachine();
    std::cout << "Done wrapped SM" << std::endl << std::endl;

    std::cout << "Test done" << std::endl;
    return 0;
}
