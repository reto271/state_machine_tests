#include <boost/mpl/list.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/statechart/event.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/state.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/transition.hpp>
#include <iostream>

namespace RetosBasicExamples {

namespace sc = boost::statechart;

// Define all events
struct EvButton12 : sc::event<EvButton12>
{
};
struct EvButton22 : sc::event<EvButton22>
{
};
struct EvButton23 : sc::event<EvButton23>
{
};
struct EvButton34 : sc::event<EvButton34>
{
};
struct EvButton45 : sc::event<EvButton45>
{
};
struct EvButton56a : sc::event<EvButton56a>
{
};
struct EvButton56b : sc::event<EvButton56b>
{
};

// We are declaring all types as structs only to avoid having to
// type public. If you don't mind doing so, you can just as well
// use class.

// We need to forward-declare the initial state because it can
// only be defined at a point where the state machine is
// defined.
struct State1;
struct State2;
struct State3;
struct State4;
struct State5;
struct State6;
struct State6a;
struct State6b;

// Boost.Statechart makes heavy use of the curiously recurring
// template pattern. The deriving class must always be passed as
// the first parameter to all base class templates.
//
// The state machine must be informed which state it has to
// enter when the machine is initiated. That's why State1 is
// passed as the second template parameter.
struct Machine : sc::state_machine<Machine, State1>
{
};

// For each state we need to define which state machine it
// belongs to and where it is located in the statechart. Both is
// specified with Context argument that is passed to
// simple_state<>. For a flat state machine as we have it here,
// the context is always the state machine. Consequently,
// Machine must be passed as the second template parameter to
// State1's base (the Context parameter is explained in more
// detail in the next example).
struct State1 : sc::simple_state<State1, Machine>
{
    // Define all transitions
    typedef sc::transition<EvButton12, State2> reactions;


    // Whenever the state machine enters a state, it creates an
    // object of the corresponding state class. The object is then
    // kept alive as long as the machine remains in the state.
    // Finally, the object is destroyed when the state machine
    // exits the state. Therefore, a state entry action can be
    // defined by adding a constructor and a state exit action can
    // be defined by adding a destructor.
    State1()
    {
        std::cout << "Enter State1" << std::endl;
    }
    ~State1()
    {
        std::cout << "Exit State1" << std::endl;
    }    // exit
};

struct State2 : sc::simple_state<State2, Machine>
{
    typedef boost::mpl::list<sc::transition<EvButton22, State2>, sc::transition<EvButton23, State3>> reactions;
    State2()
    {
        std::cout << "Enter State2" << std::endl;
    }
    ~State2()
    {
        std::cout << "Exit State2 -> jump next (sometimes skipped -> transition to itself if 34 is not implemented." << std::endl;
        post_event(EvButton34());    // Make state3 (next state) a transitory state (run-to-complete)
    }
};

struct State3 : sc::simple_state<State3, Machine>
{
    typedef sc::transition<EvButton34, State4> reactions;
    State3()
    {
        std::cout << "Enter State3" << std::endl;
    }
    ~State3()
    {
        std::cout << "Exit State3" << std::endl;
    }
};

struct State4 : sc::state<State4, Machine>
{
    typedef sc::transition<EvButton45, State5> reactions;
    State4(my_context context)
        : my_base(context)
    {
        std::cout << "Enter State4 -> transiate state" << std::endl;
        post_event(EvButton45());    // make this state transient
    }
    ~State4()
    {
        std::cout << "Exit State4" << std::endl;
    }
};

struct State5 : sc::state<State5, Machine>
{
    State5(my_context ctx)
        : my_base(ctx)
    {
        std::cout << "Enter State5" << std::endl;
    }
    ~State5()
    {
        std::cout << "Enter State5" << std::endl;
    }
    sc::result react(const EvButton56a& evt)
    {
        std::cout << "Some action" << std::endl;
        post_event(evt);
        return transit<State6>();
    }
    sc::result react(const EvButton56b& evt)
    {
        std::cout << "Other action" << std::endl;
        post_event(evt);
        return transit<State6>();
    }
    using reactions = boost::mpl::list<sc::custom_reaction<EvButton56a>, sc::custom_reaction<EvButton56b>>;
};

struct State6 : sc::simple_state<State6, Machine>
{
    State6()
    {
        std::cout << "Enter State6" << std::endl;
    }
    ~State6()
    {
        std::cout << "Exit State6" << std::endl;
    }
    sc::result react(const EvButton56a& evt)
    {
        std::cout << "transit to 6a" << std::endl;
        post_event(evt);
        return transit<State6a>();
    }
    sc::result react(const EvButton56b& evt)
    {
        std::cout << "transit to 6b" << std::endl;
        post_event(evt);
        return transit<State6b>();
    }
    using reactions = boost::mpl::list<sc::custom_reaction<EvButton56a>, sc::custom_reaction<EvButton56b>>;
};

struct State6a : sc::simple_state<State6a, Machine>
{
    State6a()
    {
        std::cout << "Enter State6a" << std::endl;
    }
    ~State6a()
    {
        std::cout << "Exit State6a" << std::endl;
    }
};

struct State6b : sc::simple_state<State6b, Machine>
{
    State6b()
    {
        std::cout << "Enter State6b" << std::endl;
    }
    ~State6b()
    {
        std::cout << "Exit State6b" << std::endl;
    }
    sc::result react(const EvButton56a& evt)
    {
        std::cout << "got msg 56a" << std::endl;
        return discard_event();
    }
    sc::result react(const EvButton56b& evt)
    {
        std::cout << "got msg 56b" << std::endl;
        return discard_event();
    }
    using reactions = boost::mpl::list<sc::custom_reaction<EvButton56a>, sc::custom_reaction<EvButton56b>>;
};

void retosExample()
{
    Machine myMachine;
    // The machine is not yet running after construction. We start
    // it by calling initiate(). This triggers the construction of
    // the initial state State1
    myMachine.initiate();

    myMachine.process_event(EvButton12());
    myMachine.process_event(EvButton12());    // event is ignored

    myMachine.process_event(EvButton22());    // Runs the exit entry functions of State 2

    myMachine.process_event(EvButton23());    // Exit state 2, state 2 has two transitions

    myMachine.process_event(EvButton45());

    myMachine.process_event(EvButton56b());

    // When we leave main(), myMachine is destructed what leads to
    // the destruction of all currently active states.
    std::cout << "Desctruct ..." << std::endl;
}

}; // namespace RetosBasicExamples
