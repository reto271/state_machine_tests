#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ../

DELETE_GTAGS=1
REGEN_GTAGS_ONLY=0
if [ 1 -eq $# ] ; then
    if [ "--noDelete" == $1 ] ; then
        DELETE_GTAGS=0
    fi
    if [ "-n" == $1 ] ; then
        DELETE_GTAGS=0
    fi
    if [ "--regenTags" == $1 ] ; then
        REGEN_GTAGS_ONLY=1
    fi
    if [ "-r" == $1 ] ; then
        REGEN_GTAGS_ONLY=1
    fi
fi

#gloabl -u does not correctly remove old symbols, hence delete the gtags files and create them anew.
#global -uv &
if [ 1 -eq ${DELETE_GTAGS} ] ; then
    echo "delete tags"
    rm -vf GPATH  GRTAGS  GTAGS
    gtags & pid_gtags=$! > /dev/null
else
    echo "No delete, just update"
fi

if [ 1 -eq ${REGEN_GTAGS_ONLY} ] ; then
    echo "Do not start emacs"
else
    emacs src/main.cpp&
fi

time wait ${pid_gtags} > /dev/null
echo ""
echo "GTAGS are ready"

# Back to the original location
popd > /dev/null
