#!/bin/bash

# Change into the script directory
SCRIPTDIR=$(readlink -f $(dirname "$0"))
pushd "${SCRIPTDIR}" > /dev/null
cd ..

if [ -e ../.clang-format ] ; then
    # Without excluding directories
    folder=src
    format_files=$(find "${folder}" -type f)

    # Exclude some directories
    # folder=src
    # exclude_folder=./submodule
    # format_files=`find "${folder}" -type f -path "${exclude_folder}" -prune

    for file in $format_files
    do
        clang-format --style=file --verbose -i "$file"
    done
else
    echo "There is no .clang-format in the project root"
    echo ""
    echo "Either copy .clang-format-reto or .clang-format-komax to .clang-format"
    echo ""
    echo "Be aware that the code format will not be the same, du etoclang-format"
    echo " version conflict"
fi

# Back to the original location
popd > /dev/null
